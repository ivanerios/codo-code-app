export type BasicResponse = {
    message: string
}

export type ErrorResponse = {
    message: string
}

export type AuthResponse = {
  _id: string,
  name: string,
  role: string,
  email: string,
  age: number,
  katas: string[],
  message: string,
    token: string
}