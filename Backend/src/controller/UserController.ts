import { Delete, Get, Put, Query, Route, Tags } from "tsoa";
import { IUserController } from "./interfaces";
import { LogError, LogSuccess, LogWarning } from "../utils/logger";

import { deleteUserByID, GetAllUsers, GetUserByID, GetUsersForCheck, updateUserByID } from "../domain/orm/User.orm";

import bodyParser from "body-parser";
import { IUser, IUserWithoutPass } from "src/domain/interfaces/IUser.interface";

let jsonParser = bodyParser.json();

@Route("/api/users")
@Tags("UserController")
export class UserController implements IUserController {
   /**
 * Endpoint to retrieve the Users of the DB
 */
  @Get("/")
  public async getUsers(@Query() page: number, @Query() limit: number, @Query() id?: string): Promise<any> {

    let response: any = '';

    if (id) {
      LogSuccess('[/api/users] Get User by ID Request')
      response = await GetUserByID(id);
    } else {

      if(!page){
        LogSuccess('[/api/users] Get All Users Request')
        response = await GetUsersForCheck();
      } else {
      LogSuccess('[/api/users] Get All Users Request')
      response = await GetAllUsers(page, limit);
    }
    }

    return response;


  }

  /**
* Endpoint to update the Users of the DB
*/
  @Put("/")
  public async updateUser(@Query() id: string, user: IUser | IUserWithoutPass): Promise<any> {
        LogSuccess('[/api/users] Update a User')
    let response: any = '';

    if (id) {
            await updateUserByID(id, user).then((r) => {
        response = {
          status: 204,
          message: `User with id ${id} updated successfully`
        }
      });
    } else {
      LogWarning('[/api/users] Update User WITHOUT ID Request')
      response = {
        status: 400,
        message: 'Please, provide an ID to update from database'
      }
    }

    return response;
  }
  /**
 * Endpoint to delete the Users of the DB
 */

  @Delete("/")
  public async deleteUser(@Query() id?: string): Promise<any> {

    let response: any = '';

    if (id) {
      LogSuccess(`[/api/users] Delete User by ID: ${id}`)
      await deleteUserByID(id).then((r) => {
        response = {
          status: 204,
          message: `User with id ${id} deleted successfully`
        }
      });
    } else {
      LogWarning('[/api/users] Delete User WITHOUT ID Request')
      response = {
        status: 400,
        message: 'Please, provide an ID to remove from database'
      }
    }

    return response;


  }

}