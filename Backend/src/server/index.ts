import express, { Express, Request, Response } from "express";
import swaggerUi from 'swagger-ui-express';
import cors from 'cors';
import helmet from 'helmet';

import rootRouter from "../routes";
import mongoose from "mongoose";


// Create Express APP
const server: Express = express();

mongoose.connect('mongodb://localhost:27017/codeverification')

server.use(
    '/docs',
    swaggerUi.serve,
    swaggerUi.setup(undefined, {
        swaggerOptions: {
            url: '/swagger.json',
            explorer: true
        }
    })
);

server.use(
    '/api',
    rootRouter
)

server.use(express.static('public'));




server.use(helmet());
server.use(cors());

server.use(express.urlencoded({ extended: true, limit: '50mb' }))
server.use(express.json({limit: '50mb'}))

server.get('/', (req: Request, res: Response) => {
    res.redirect('/api');
});

export default server;