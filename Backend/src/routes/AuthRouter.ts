import express, { Request, Response } from "express";
import { AuthController } from "../controller/AuthController";
import { LogInfo } from "../utils/logger";
import { IUser } from "../domain/interfaces/IUser.interface";
import { IAuth } from "../domain/interfaces/IAuthInterface";

import bcrypt from 'bcrypt';

import { verifyToken } from '../middlewares/verifyToken'

import bodyParser from "body-parser";

let jsonParser = bodyParser.json();


let authRouter = express.Router();

authRouter.route('/register')
  .post(jsonParser, async (req: Request, res: Response) => {

    let { name, role, email, password, age, katas, attempts } = req?.body;
    let hashedPassword = '';

    if (name && role && email && password && age) {
      hashedPassword = bcrypt.hashSync(password, 8);

      let newUser: IUser = {
        name,
        role,
        email,
        password: hashedPassword,
        age,
        katas,
        attempts
      }

      const controller: AuthController = new AuthController();
      const response = await controller.registerUser(newUser);


      return res.status(200).send(response);
    } else {
      return res.status(400).send({
        message: '[ERROR User Data missing] No user can be registered'
      });
    }
  })

authRouter.route('/login')
  .post(jsonParser, async (req: Request, res: Response) => {

    let { email, password } = req?.body;

    if (email && password) {

      const controller: AuthController = new AuthController();

      let auth: IAuth = {
        email: email,
        password: password
      }

      const response = await controller.loginUser(auth);


      return res.status(200).send(response);
    } else {
      return res.status(400).send({
        message: '[ERROR User Data missing] No user can be logged'
      });
    }
  })

export default authRouter;