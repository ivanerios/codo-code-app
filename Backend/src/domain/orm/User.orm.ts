import { userEntity } from "../entitites/User.entity";
import { LogSuccess, LogError } from "../../utils/logger";
import { IUser, IUserWithoutPass } from "../interfaces/IUser.interface";
import { UserResponse } from "../types/UserResponse.type";
import { kataEntity } from "../entitites/Kata.entity";
import { IKata } from "../interfaces/IKata.interface";
import mongoose from "mongoose";

import dotenv from 'dotenv';

dotenv.config();

// CRUD

/**
 * Method to obtain all Users from Collection "Users" in Mongo Server
 */
export const GetAllUsers = async (page: number, limit: number): Promise<any[] | undefined> => {
    try {
        let userModel = userEntity();

        let response: any = {};

        await userModel.find({ isDelete: false }).select('name role email age katas attempts').limit(limit).skip((page - 1) * limit)
            .exec().then((users: IUser[]) => {

                response.users = users;
            });

        await userModel.countDocuments().then((total: number) => {
            response.totalPages = Math.ceil(total / limit);
            response.currentPage = page;
        });

        return response;
    } catch (error) {
        LogError(`[ORM ERROR]: Getting All Users: ${error}`);
    }

}

export const GetUsersForCheck = async (): Promise<any[] | undefined> => {
  try {
      let userModel = userEntity();
      return await userModel.find({ isDelete: false })
  } catch (error) {
      LogError(`[ORM ERROR]: Getting All Users: ${error}`);
  }

}

export const GetUserByID = async (id: string): Promise<any | undefined> => {
    try {
        let userModel = userEntity();

        return await userModel.findById(id).select('name role email age katas attempts');
    } catch (error) {
        LogError(`[ORM ERROR]: Getting User by ID: ${error}`);
    }
}

export const deleteUserByID = async (id: string): Promise<any | undefined> => {
    try {
        let userModel = userEntity();

        return await userModel.deleteOne({ _id: id });
    } catch (error) {
        LogError(`[ORM ERROR]: Deleting User by ID: ${error}`);
    }
}

export const updateUserByID = async (id: string, user: IUser | IUserWithoutPass): Promise<any | undefined> => {
      try {
        let userModel = userEntity();
        return await userModel.findByIdAndUpdate(id, user)
    } catch (error) {
        LogError(`[ORM ERROR]: Updating User: ${error}`);
    }
}


