import { IKata } from "./IKata.interface";

export enum UserRole {
  USER = 'User',
  ADMIN = 'Admin'
}

export interface IUser {
    name: string,
    role: UserRole,
    email: string,
    age: number,
    password: string,
    katas: number,
    attempts: number
}

export interface IUserWithoutPass {
  name: string,
  role: UserRole,
  email: string,
  age: number,
  katas: number,
  attempts: number
}