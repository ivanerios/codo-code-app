import mongoose from "mongoose";
import { IUser, IUserWithoutPass } from "../interfaces/IUser.interface";
export const userEntity = () => {

    let userSchema = new mongoose.Schema<IUser | IUserWithoutPass>({
        name: {type: String, required: true},
        role: {type: String, required: true},
        email: {type: String, required: true},
        age: {type: Number, required: true},
        password: {type: String, required: false},
        katas: {type: Number, required: true},
        attempts: {type: Number, required: true}
    })

    return mongoose.models.Users || mongoose.model<IUser | IUserWithoutPass>('Users', userSchema)
}