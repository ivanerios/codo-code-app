export type Props = {
  children: Object,
}

export type UserType = {
  userInfo: {_id: string,
  name: string,
  role: string,
  email: string,
  age: number,
  katas: number,
attempts: number},
  setUserInfo: (userInfo: any) => void
}

export type UserInfoType = {
  _id: string,
  name: string,
  role: string,
  email: string,
  age: number,
  katas: number,
  attempts: number
}