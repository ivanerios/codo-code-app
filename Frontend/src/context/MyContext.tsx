import { createContext, useState, useContext } from "react";
import { Props, UserType } from "./types";

export const MyContext = createContext<UserType>({
  userInfo: {_id: '',
  name: '',
  role: '',
  email: '',
  age: 0,
  katas: 0,
attempts: 0},
  setUserInfo: () => { }
})

export default MyContext