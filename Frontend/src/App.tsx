import React, { useState } from 'react';
import './App.css';

import { BrowserRouter as Router} from 'react-router-dom';
import { AppRoutes } from './routes/routes';
import MyContext from './context/MyContext';

const App = (props: any) => {

  const initialState = {
    _id: '',
    name: '',
    role: '',
    email: '',
    age: 0,
    katas: 0,
    attempts: 0
  }

  const [userInfo, setUserInfo] = useState(initialState)

    return (
    <div className="App">
      <MyContext.Provider value={{userInfo, setUserInfo}}>
      <Router>
       <AppRoutes />
      </Router>
      </MyContext.Provider>
    </div>
  );
}

export default App;
