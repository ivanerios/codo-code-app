import react from 'react';

import Avatar from '@mui/material/Avatar';
import CssBaseline from '@mui/material/CssBaseline';
import Link from '@mui/material/Link';
import Box from '@mui/material/Box';

import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { lightGreen } from '@mui/material/colors';
import LoginForm from './LoginForm';
import { Container, Grid, Paper } from '@mui/material';

import logo from '../../img/CODO_LOGO.svg'

const theme = createTheme({
  palette: {
    background: {
      default: "#FFFFFF"
    },
    primary: lightGreen,
    secondary: {
      main: '#555555',
    },
  },
});

export const LoginMaterial = () => {

  return (
    <ThemeProvider theme={theme}>
      <Container maxWidth="xs" sx={{ height: '100vh' }}>
        <CssBaseline />
        <Box
          sx={{
            margin: 0,
            position: 'relative',
            top: '50%',
            transform: 'translateY(-50%)',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <img style={{width: "250px", marginBottom: "20px"}} src={logo} />
          <Avatar sx={{ m: 1, bgcolor: 'primary' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Login
          </Typography>
          <Box sx={{ mt: 3 }}>
            <Grid container >
              <LoginForm />
              </Grid>
              <Grid container justifyContent="flex-end">
              <Grid item>
                  <Link href="/register" variant="body2">
                    {"Don't have an account? Sign Up"}
                  </Link>
                </Grid>
              </Grid>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  );
}