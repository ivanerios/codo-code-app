import React, { useEffect, useState } from "react";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from 'yup';

import { register } from "../../services/authService";
import { AxiosResponse } from "axios";
import { Box, Button, TextField } from "@mui/material";

import axios from '../../utils/config/axios.config';
import { useNavigate } from "react-router-dom";
import { useSessionStorage } from "../../hooks/useSessionStorage";

const RegisterForm = () => {

  let loggedIn = useSessionStorage('sessionJWTToken');

  const initialValues = {
    name: '',
    role: 'User',
    email: '',
    password: '',
    confirm: '',
    age: 18,
    katas: 0,
    attempts: 0
  }

  const [names, setNames] = useState<any[]>([])
  const [emails, setEmails] = useState<any[]>([])
  const getUserList = () => {
    axios.get('/users').then((response) => {
      const allUsers = response.data.users;
      allUsers.forEach((user: { email: string; name: string }) => {
        setEmails(emails => [...emails, user.email])
        setNames(names => [...names, user.name])
      });

    }).catch(error => console.error(`Error: ${error}`))
  }

  useEffect(() => {
    getUserList();
  }, [])

  let navigate = useNavigate();

  //Yup Validation Schema
  const registerSchema = Yup.object().shape(
    {
      name: Yup.string().min(6, 'Username must have 6 letters minimun').test('name', 'User name already exists', (value: string | undefined) => !names.includes(value)).required('Username is required'),
      role: Yup.string(),
      email: Yup.string().email('invalid email format').test('email', 'User email already exists', (value: string | undefined) => !emails.includes(value)).required('Email is required'),
      password: Yup.string().min(8, 'Password too short').required('password is required'),
      confirm: Yup.string().when("password", {
        is: (value: string) => (value && value.length > 0 ? true : false),
        then: Yup.string().oneOf(
          [Yup.ref("password")], 'Passwords must match'
        )
      }).required('You must confirm your password'),
      age: Yup.number().min(10, 'You must be over 10 years old').required('Age is required')

    }
  );

  return (
    <div>
      <Formik
        initialValues={initialValues}
        validationSchema={registerSchema}
        onSubmit={async (values) => {


          register(values.name, values.role, values.email, values.password, values.age, values.katas, values.attempts).then((response: AxiosResponse) => {
            if (response.status == 200) {
              console.log('User registered correctly')
              console.log(response.data)
              alert('User registered correctly')
              if(!loggedIn){
              navigate('/login')
            }else{navigate('/users')}
            } else {
              throw new Error('Error in registry')
            }
          }).catch((error) => console.error('Register Error')
          )
        }}
      >

        {
          ({ values, touched, errors, isSubmitting, handleChange, handleBlur }) => (
            <Form>

              <TextField
                fullWidth
                id='name' label='name' type='name' name='name'
                value={values.name}
                onChange={handleChange} onBlur={handleBlur} />
              {
                errors.name && touched.name && (
                  <ErrorMessage className="error" name='name' component='div'></ErrorMessage>
                )
              }


              <TextField
                sx={{
                  mt: 1,
                  mb: 1
                }}
                fullWidth
                id='email' label='email' type='email' name='email'
                value={values.email}
                onChange={handleChange} onBlur={handleBlur} />
              {
                errors.email && touched.email && (
                  <ErrorMessage className="error" name='email' component='div'></ErrorMessage>
                )
              }


              <TextField
                fullWidth
                id='password' label='password' type='password' name='password'
                value={values.password}
                onChange={handleChange} onBlur={handleBlur} />
              {
                errors.password && touched.password && (
                  <ErrorMessage className="error" name='password' component='div'></ErrorMessage>
                )
              }

              <TextField
                sx={{
                  mt: 1,
                  mb: 1
                }}
                fullWidth
                id='confirm' label='confirm password' type='password' name='confirm'
                value={values.confirm}
                onChange={handleChange} onBlur={handleBlur} />
              {
                errors.confirm && touched.confirm && (
                  <ErrorMessage className="error" name='confirm' component='div'></ErrorMessage>
                )
              }


              <TextField

                fullWidth
                id='age' label='age' type='number' name='age'
                value={values.age}
                onChange={handleChange} onBlur={handleBlur} />
              {
                errors.age && touched.age && (
                  <ErrorMessage className="error" name='age' component='div'></ErrorMessage>
                )
              }

              <Button sx={{
                mt: 1,
                mb: 1
              }} fullWidth color="primary" variant="contained" type='submit'>Register User</Button>
              {
                isSubmitting ? (<p>Sending Data to Registry...</p>) : null
              }
            </Form>
          )
        }


      </Formik>
    </div>
  )
}


export default RegisterForm