import React, { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";

import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from 'yup';

import { login } from "../../services/authService";
import { AxiosResponse } from "axios";
import { Button, TextField } from "@mui/material";
import MyContext from "../../context/MyContext";
import { UserInfoType } from "../../context/types";

// Define Schema of validation with Yup
const loginSchema = Yup.object().shape(
  {
    email: Yup.string().email('Invalid Email Format').required('Email is required'),
    password: Yup.string().required('Password is required')
  }
);

//Login Component
const LoginForm = () => {

  const { userInfo, setUserInfo } = useContext(MyContext)

  // We define the initial credentials
  const initialCredentials = {
    email: '',
    password: ''
  }

  const saveUserInfo = (userInform: UserInfoType) => {
    setUserInfo(userInform)
}

  let navigate = useNavigate();

  return (
    <div>
      <Formik
        initialValues={initialCredentials}
        validationSchema={loginSchema}
        onSubmit={async (values) => {
          login(values.email, values.password).then(async (response: AxiosResponse) => {
            if (response.status == 200) {
              await sessionStorage.setItem('sessionJWTToken', response.data.token);

              await sessionStorage.setItem('userId', response.data._id);

              await saveUserInfo({_id: response.data._id,
              name: response.data.name,
              role: response.data.role,
              email: response.data.email,
              age: response.data.age,
              katas: response.data.katas,
            attempts: response.data.attempts});
              navigate('/')

            } else {
              throw new Error('Error in server')
            }
          }).catch((error) => console.error(`Its an error: ${error}`)
          )
        }}
      >
        {
          ({ values, touched, errors, isSubmitting, handleChange, handleBlur }) => (
            <Form>
              
                <TextField
                  
                  fullWidth
                  id="email"
                  name="email"
                  label="Email"
                  value={values.email}
                  onChange={handleChange}
                />
                {
                  errors.email && touched.email && (
                    <ErrorMessage className="error" name='email' component='div'></ErrorMessage>
                  )
                }


                <TextField
                  sx={{
                    mt: 1,
                    mb: 1
                  }}
                  fullWidth
                  id="password"
                  name="password"
                  label="Password"
                  type="password"
                  value={values.password}
                  onChange={handleChange}
                />
                {
                  errors.password && touched.password && (
                    <ErrorMessage className="error" name='password' component='div'></ErrorMessage>
                  )
                }

                <Button  sx={{
                    mb: 1
                  }} fullWidth color="primary" variant="contained" type="submit">
                  Submit
                </Button>
                {
                  isSubmitting ? (<p>Checking credentials...</p>) : null
                }
              
            </Form>
          )
        }

      </Formik>
    </div>
  )

}

export default LoginForm
