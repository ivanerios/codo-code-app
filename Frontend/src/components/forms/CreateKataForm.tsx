import React, { useContext, useEffect, useState } from "react";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from 'yup';

import { register } from "../../services/authService";
import { AxiosResponse } from "axios";
import { Box, Button, FormControl, FormHelperText, InputLabel, MenuItem, TextField } from "@mui/material";

import axios from '../../utils/config/axios.config';
import { useNavigate } from "react-router-dom";
import { useSessionStorage } from "../../hooks/useSessionStorage";
import MyContext from "../../context/MyContext";
import { createKata } from "../../services/katasService";

import Select, { SelectChangeEvent } from '@mui/material/Select';
import { getUserByID, updateUserWithoutPass } from "../../services/usersService";
import { User } from "../../utils/types/User.type";

const CreateKataForm = () => {

  let loggedIn = useSessionStorage('sessionJWTToken');
  let sessionIn = useSessionStorage('userId');

  const { userInfo, setUserInfo } = useContext(MyContext)

  let userInitialValues = 
    {_id: '',
      name: '',
      role: '',
      email: '',
      age: 18,
      katas: 0,
      attempts: 0,
    language: ''}

      const [user, setUser] = useState<User>(userInitialValues)

  useEffect(() => {
    getUserByID(loggedIn, sessionIn).then(async (response: AxiosResponse) => {
      let userInform = {_id: response.data._id,
        name: response.data.name,
        role: response.data.role,
        email: response.data.email,
        age: response.data.age,
        katas: response.data.katas + 1,
        attempts: response.data.attempts}

      await setUserInfo(userInform)
      await setUser(userInform)
    
    })
  }, [])

  const initialValues = {
    name: '',
    description: '',
    level: 'Basic',
    intents: 0,
    stars: 0,
    creator: userInfo._id,
    solution: '',
    participants: [''],
    language: ''
  }

  let navigate = useNavigate();

  //Yup Validation Schema
  const registerSchema = Yup.object().shape(
    {
      name: Yup.string().min(6, 'The name of the kata must have 6 letters minimun').required('Kata name is required'),
      description: Yup.string().min(6, 'The description of the kata must have 6 letters minimun').required('Description is required'),
      level: Yup.string(),
      solution: Yup.string().min(8, 'The solution of the kata must have 6 letters minimun').required('Solution is required'),
    }
  );

  const [level, setLevel] = useState('Basic');
  const [language, setLanguage] = useState('javascript');

  const handleFormChange = (event: SelectChangeEvent) => {
    setLevel(event.target.value);
  };
  const handleLanguageChange = (event: SelectChangeEvent) => {
    setLanguage(event.target.value);
  };

  return (
    <div>
      <Formik
        enableReinitialize={true}
        initialValues={initialValues}
        validationSchema={registerSchema}
        onSubmit={async (values) => {
          updateUserWithoutPass(loggedIn, user._id, user.name, user.email, user.role, user.age, user.katas, user.attempts)
          createKata(loggedIn, values.name, values.description, level, values.intents, values.stars, values.creator, values.solution, values.participants, language).then((response: AxiosResponse) => {
            if (response.status == 200) {
              alert('Kata created correctly')
              if (!loggedIn) {
                navigate('/login')
              } else { navigate('/katas') }
            } else {
              throw new Error('Error in creation')
            }
          }).catch((error) => console.error('Creation Error')
          )
        }}
      >

        {
          ({ values, touched, errors, isSubmitting, handleChange, handleBlur }) => (
            <Form>

              <TextField
                fullWidth
                id='name' label='name' type='name' name='name'
                value={values.name}
                onChange={handleChange} onBlur={handleBlur} />
              {
                errors.name && touched.name && (
                  <ErrorMessage className="error" name='name' component='div'></ErrorMessage>
                )
              }


              <TextField
                sx={{
                  mt: 1,
                  mb: 1
                }}
                multiline
                rows={6}
                fullWidth
                id='description' label='description' type='description' name='description'
                value={values.description}
                onChange={handleChange} onBlur={handleBlur} />
              {
                errors.description && touched.description && (
                  <ErrorMessage className="error" name='description' component='div'></ErrorMessage>
                )
              }
              <Select sx={{
                mt: 1,
                mb: 1
              }}
                fullWidth
                id='level'
                value={level}
                onChange={handleFormChange}>
                <MenuItem value={'Basic'}>Basic</MenuItem>
                <MenuItem value={'Medium'}>Medium</MenuItem>
                <MenuItem value={'High'}>High</MenuItem>
              </Select>

              <Select sx={{
                mt: 1,
                mb: 1
              }}
                fullWidth
                id='language'
                value={language}
                onChange={handleLanguageChange}>
                <MenuItem value={'javascript'}>Javascript</MenuItem>
                <MenuItem value={'typescript'}>Typescript</MenuItem>
                <MenuItem value={'csharp'}>C#</MenuItem>
                <MenuItem value={'go'}>Go</MenuItem>
                <MenuItem value={'java'}>Java</MenuItem>
                <MenuItem value={'kotlin'}>Kotlin</MenuItem>
                <MenuItem value={'php'}>PHP</MenuItem>
                <MenuItem value={'ruby'}>Ruby</MenuItem>
                <MenuItem value={'jsx'}>JSX</MenuItem>
                <MenuItem value={'tsx'}>TSX</MenuItem>
              </Select>

              <TextField
                fullWidth
                multiline
                rows={6}
                id='solution' label='solution' type='solution' name='solution'
                value={values.solution}
                onChange={handleChange} onBlur={handleBlur} />
              {
                errors.solution && touched.solution && (
                  <ErrorMessage className="error" name='solution' component='div'></ErrorMessage>
                )
              }

              <Button sx={{
                mt: 1,
                mb: 1
              }} fullWidth color="primary" variant="contained" type='submit'>Create Kata</Button>
              {
                isSubmitting ? (<p>Sending Data to Registry...</p>) : null
              }
            </Form>
          )
        }


      </Formik>
    </div>
  )
}


export default CreateKataForm