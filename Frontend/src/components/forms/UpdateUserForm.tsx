import React, { useContext, useEffect, useState } from "react";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from 'yup';

import { AxiosResponse } from "axios";
import { Button, FormControl, InputLabel, MenuItem, Select, SelectChangeEvent, TextField } from "@mui/material";

import axios from '../../utils/config/axios.config';
import { useNavigate, useParams } from "react-router-dom";
import { User } from "../../utils/types/User.type";
import { getUserByID, updateUserByID } from "../../services/usersService";
import { useSessionStorage } from "../../hooks/useSessionStorage";
import MyContext from "../../context/MyContext";

const UpdateUserForm = () => {

  const { userInfo, setUserInfo } = useContext(MyContext)

  let loggedIn = useSessionStorage('sessionJWTToken');
  let sessionIn = useSessionStorage('userId');

  const [isAdmin, setIsAdmin] = useState<boolean>(false)

  useEffect(() => {
    userInfo.role === 'Admin' ? setIsAdmin(true) : setIsAdmin(false)
  }, [isAdmin])

  useEffect(() => {
    getUserByID(loggedIn, sessionIn).then(async (response: AxiosResponse) => {
      await setUserInfo({_id: response.data._id,
      name: response.data.name,
      role: response.data.role,
      email: response.data.email,
      age: response.data.age,
      katas: response.data.katas,
      attempts: response.data.attempts})})
  }, [])

  let { id } = useParams();

  const [user, setUser] = useState<User | undefined>(undefined);

  const [role, setRole] = useState('User')

    useEffect(() => {
    if (!loggedIn) {
      return navigate('/login');
    } else {
      if (id) {
        getUserByID(loggedIn, id).then((response: AxiosResponse) => {
          if (response.status === 200 && response.data) {
            let userData: User = {
              _id: response.data._id,
              name: response.data.name,
              email: response.data.email,
              role: response.data.role,
              age: response.data.age,
              katas: response.data.katas,
              attempts: response.data.attempts
            }

            setUser(userData);
            setRole(response.data.role);
            console.log(userData)
          }

        }).catch((error) => console.error('Kata By ID error'))
      } else {

      }

    }
  }, [loggedIn])

  const initialValues = {
    name: user?.name,
    email: user?.email,
    role: user?.role,
    age: user?.age,
    password: '',
    confirm: '',
    katas: user?.katas,
    attempts: user?.attempts
  }

  let navigate = useNavigate();

  //Yup Validation Schema
  const registerSchema = Yup.object().shape(
    {
      name: Yup.string().min(6, 'Username must have 6 letters minimun').required('Username is required'),
      email: Yup.string().email('invalid email format').required('Email is required'),
      password: Yup.string().min(8, 'Password too short').required('password is required'),
      confirm: Yup.string().when("password", {
        is: (value: string) => (value && value.length > 0 ? true : false),
        then: Yup.string().oneOf(
          [Yup.ref("password")], 'Passwords must match'
        )
      }).required('You must confirm your password'),
      age: Yup.number().min(10, 'You must be over 10 years old').required('Age is required')

    }
  );

  const handleFormChange = (event: SelectChangeEvent) => {
    setRole(event.target.value);
  };

  return (
    <div>
      <Formik
        enableReinitialize={true}
        initialValues={initialValues}
        validationSchema={registerSchema}
        onSubmit={async (values) => {

          updateUserByID(loggedIn, id, values.name, values.email, role, values.age, values.password, values.katas, values.attempts).then((response: AxiosResponse) => {
            if (response.status == 200) {
              alert('User registered correctly')
              if(!loggedIn){
              navigate('/login')
            }else{navigate('/users')}
            } else {
              throw new Error('Error in registry')
            }
          }).catch((error) => console.error('Register Error')
          )
        }}
      >

        {
          ({ values, touched, errors, isSubmitting, handleChange, handleBlur, handleReset }) => (
            <Form>

              <TextField
                fullWidth
                InputLabelProps={{ shrink: true }}
                id='name' label='name' type='name' name='name'
                value={values.name}
                onChange={handleChange} onBlur={handleBlur} />
              {
                errors.name && touched.name && (
                  <ErrorMessage className="error" name='name' component='div'></ErrorMessage>
                )
              }


              <TextField
                sx={{
                  mt: 1,
                  mb: 1
                }}
                fullWidth
                InputLabelProps={{ shrink: true }}
                id='email' label='email' type='email' name='email'
                value={values.email}
                onChange={handleChange} onBlur={handleBlur} />
              {
                errors.email && touched.email && (
                  <ErrorMessage className="error" name='email' component='div'></ErrorMessage>
                )
              }

              {isAdmin ? 
                <Select sx={{
                  mt: 1,
                  mb: 1
                }}
                  fullWidth
                  id='role'
                  value={role}
                  onChange={handleFormChange}>
                  <MenuItem value={'User'}>User</MenuItem>
                  <MenuItem value={'Admin'}>Admin</MenuItem>
                </Select> : null}

              <TextField
                fullWidth
                id='password' label='password' type='password' name='password'
                value={values.password}
                onChange={handleChange} onBlur={handleBlur} />
              {
                errors.password && touched.password && (
                  <ErrorMessage className="error" name='password' component='div'></ErrorMessage>
                )
              }


              <TextField
                sx={{
                  mt: 1,
                  mb: 1
                }}
                fullWidth
                id='confirm' label='confirm password' type='password' name='confirm'
                value={values.confirm}
                onChange={handleChange} onBlur={handleBlur} />
              {
                errors.confirm && touched.confirm && (
                  <ErrorMessage className="error" name='confirm' component='div'></ErrorMessage>
                )
              }

              <TextField

                fullWidth
                InputLabelProps={{ shrink: true }}
                id='age' label='age' type='number' name='age'
                value={values.age}
                onChange={handleChange} onBlur={handleBlur} />
              {
                errors.age && touched.age && (
                  <ErrorMessage className="error" name='age' component='div'></ErrorMessage>
                )
              }

              <Button sx={{
                mt: 1,
                mb: 1
              }} fullWidth color="primary" variant="contained" type='submit'>Update User</Button>
              {
                isSubmitting ? (<p>Sending Data to Registry...</p>) : null
              }
            </Form>
          )
        }


      </Formik>
    </div>
  )
}


export default UpdateUserForm