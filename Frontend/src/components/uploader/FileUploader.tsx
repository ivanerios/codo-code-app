import { useState } from 'react';
import { Dropzone, FileItem, FileValidated, FullScreenPreview, VideoPreview } from "@dropzone-ui/react";

export const FileUploader = () => {
  const [files, setFiles] = useState<FileValidated[]>([])
  const [imageSrc, setImageSrc] = useState<any>(undefined);
  const [videoSrc, setVideoSrc] = useState<any>(undefined);

  const updateFiles = (incomingFiles: FileValidated[]) => {
    setFiles(incomingFiles);
    incomingFiles.forEach((file: FileValidated) => {
      file.uploadMessage = 'File correctly'
    })
  };

  const handleSee = (imageSource: any) => {
    setImageSrc(imageSource);
  };
  const handleClean = (files: FileValidated) => {
    console.log("list cleaned", files);
  };

  const handleWatch = (vidSrc: any) => {
   setVideoSrc(vidSrc);
  };

  const handleUpload=(response: any) => {

  }

  const removeFile = (id: string | number | undefined) => {
   if(id){
    setFiles(files.filter((x) => x.id !== id))}
  };
  return (
    <Dropzone
    style={{minWidth: "505px"}}
    onChange={updateFiles}
    value={files}
    maxFiles={5}
   // maxFileSize={2998000}
    url="http://localhost:8000/api/katas/upload"
    //fakeUploading
    onUploadFinish={handleUpload}
        >
      {files.map((file) => (
        <FileItem {...file}
        preview key={file.id} info onDelete={() => removeFile(file.id)}
        resultOnTooltip hd localization='ES-es' onSee={handleSee} onWatch={handleWatch} />
      ))}
      <FullScreenPreview
        imgSource={imageSrc}
        openImage={imageSrc}
        onClose={(e:any) => handleSee(undefined)}
      />
      <VideoPreview
        videoSrc={videoSrc}
        openVideo={videoSrc}
        onClose={(e: any) => handleWatch(undefined)}
        controls
        autoplay
      />
    </Dropzone>
  );
}