import React, { useContext, useEffect, useState } from 'react';
import { styled, CssBaseline, Drawer, AppBar, AppBarProps, Toolbar, List, Divider, IconButton, Badge, Typography } from '@mui/material';
import { Menu, ChevronLeft, Logout, Person } from '@mui/icons-material';

import { MenuItems } from './MenuItems';

import { Link, useNavigate } from 'react-router-dom';
import MyContext from '../../context/MyContext';

import symbol from '../../img/CODO_SYMBOL_WHITE.svg'
import { lightGreen } from '@mui/material/colors';

const drawerWidth: number = 200;

interface MyAppBarProps extends AppBarProps {
  open?: boolean
}

const MyAppBar = styled(AppBar, {
  shouldForwardProp: (prop) => prop !== 'open'
})<MyAppBarProps>(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(['width', 'margin'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  })
}));

const MyDrawer = styled(Drawer, {
  shouldForwardProp: (prop) => prop !== 'open'
})(
  ({ theme, open }) => ({
    '& .MuiDrawer-paper': {
      position: 'relative',
      whiteSpace: 'nowrap',
      width: drawerWidth,
      transition: theme.transitions.create(['width'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen
      }),
      boxSizing: 'border-box',
      ...(!open && {
        overflowX: 'hidden',
        transition: theme.transitions.create(['width', 'margin'], {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen
        }),
        width: theme.spacing(7)
      })
    }
  })
);

export const MenuDashboard = () => {

  const { userInfo, setUserInfo } = useContext(MyContext)

  let navigate = useNavigate();

  const [isAdmin, setIsAdmin] = useState<boolean>(false)
  
  useEffect(() => {
    userInfo.role === 'Admin' ? setIsAdmin(true) : setIsAdmin(false)
  },[isAdmin])
  
  const [open, setOpen] = useState(false);

  const toggleDrawer = () => {
    setOpen(false);
  }

  const logout = () => {
    sessionStorage.setItem('sessionJWTToken', '')
    setUserInfo({
      userInfo: {
        _id: '',
        name: '',
        role: '',
        email: '',
        age: 0,
        katas: ['']
      }
    })
    navigate('/')
  }
  
  const navigateToDashboard = (id: string) => {
    navigate('/')
  }

  return (
    <div>
      <CssBaseline />
      <MyAppBar position='absolute' open={open}>
        <Toolbar sx={{ pr: '24px' }}>
          <Typography component='h6' variant='h6' color='inherit' noWrap sx={{ flexGrow: 1 }}>
            <Link to='/'><img style={{ width: '120px', display: 'block', marginRight: 'auto' }} src={symbol} /></Link>
          </Typography>
          <IconButton  onClick={() => navigateToDashboard(userInfo._id)} color='secondary'>
              <Person />
          </IconButton>
          <IconButton color='secondary'>
            <Logout onClick={() => { logout() }} />
          </IconButton>
        </Toolbar>
      </MyAppBar>
      <MyDrawer variant='permanent' open={open} sx={{ height: '100vh' }}>
        <Toolbar sx={{ display: 'flex', alignItems: 'center', justifyContent: 'flex-end', px: [1] }}>
          <IconButton color='inherit' onClick={toggleDrawer}>
            <ChevronLeft />
          </IconButton>
        </Toolbar>
        <Divider />
        <List component='nav'>
          <MenuItems />
        </List>
      </MyDrawer>
    </div>
  )
}