import { AxiosResponse } from "axios";
import react, { useContext, useEffect, useState } from "react";

import { Link, useNavigate, useParams } from "react-router-dom";
import MyContext from "../context/MyContext";

import { createTheme, ThemeProvider, Box, Toolbar, Container, Grid, Paper, Typography, IconButton, Divider, TableContainer, Table, TableBody, TableRow, TableCell, styled, Button, Badge } from '@mui/material';
import { lightGreen } from '@mui/material/colors';
import { MenuDashboard } from '../components/dashboard/MenuDashboard';

import { useSessionStorage } from "../hooks/useSessionStorage";
import { getAllUsers, getUserByID } from "../services/usersService";
import { User } from "../utils/types/User.type";
import { Kata } from "../utils/types/Kata.type";
import { getAllKatasSorted, getKataByID } from "../services/katasService";
import { AddCircle, EmojiEvents, People, Person, SportsMartialArts, Star } from "@mui/icons-material";

import logo from '../img/CODO_LOGO.svg';
import bambu from '../img/Bambu1.jpg';
import dojo from '../img/dojo-japones.jpg'

const myTheme = createTheme({
  palette: {
    primary: lightGreen,
    secondary: {
      main: '#FFFFFF',
    },
  },
});

export const WelcomePage = () => {

  const navigateToKataCreate = () => {
    navigate('/create/kata')
  }

  const navigateToUserUpdate = (id: string) => {
    navigate(`/update/user/${id}`)
  }

  const { userInfo, setUserInfo } = useContext(MyContext)

  useEffect(() => {
    getUserByID(loggedIn, sessionIn).then(async (response: AxiosResponse) => {
      await setUserInfo({
        _id: response.data._id,
        name: response.data.name,
        role: response.data.role,
        email: response.data.email,
        age: response.data.age,
        katas: response.data.katas,
        attempts: response.data.attempts
      })
    })
  }, [])


  let loggedIn = useSessionStorage('sessionJWTToken');
  let sessionIn = useSessionStorage('userId');
  let navigate = useNavigate();

  const [isAdmin, setIsAdmin] = useState<boolean>(false)

  useEffect(() => {
    userInfo.role === 'Admin' ? setIsAdmin(true) : setIsAdmin(false)
  }, [isAdmin])

  let { id } = useParams();

  const [kata, setKata] = useState<Kata | undefined>(undefined);

  useEffect(() => {
    if (!loggedIn) {
      return navigate('/login');
    } else {
      if (id) {
        getKataByID(loggedIn, id).then((response: AxiosResponse) => {
          if (response.status === 200 && response.data) {
            let kataData: Kata = {
              _id: response.data._id,
              name: response.data.name,
              description: response.data.description,
              stars: response.data.stars,
              level: response.data.level,
              intents: response.data.intents,
              creator: response.data.creator,
              solution: response.data.solution,
              participants: response.data.participants,
              language: response.data.language
            }

            setKata(kataData);

            console.table(kata)
          }

        }).catch((error) => console.error('Kata By ID error'))
      } else {

      }

    }
  }, [loggedIn])

  const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }));

  const [katasByStars, setKatasByStars] = useState([]);
  const [katasByIntents, setKatasByIntents] = useState([]);

  const getKatasByIntents = () => {
    getAllKatasSorted(loggedIn, 3, 1, 'intents').then((response: AxiosResponse) => {
      if (response.status === 200 && response.data.katas && response.data.currentPage && response.data.totalPages) {
        let katas = response.data.katas
        setKatasByIntents(katas);
      } else {
        throw new Error('Error obtaining Katas')
      }
    }).catch((error) => console.error('Get All Users Error'))
  }

  const getKatasByStars = () => {
    getAllKatasSorted(loggedIn, 3, 1, 'stars').then((response: AxiosResponse) => {
      if (response.status === 200 && response.data.katas && response.data.currentPage && response.data.totalPages) {
        let katas = response.data.katas
        setKatasByStars(katas);
      } else {
        throw new Error('Error obtaining Katas')
      }
    }).catch((error) => console.error('Get All Users Error'))
  }

  const navigateToKataDetail = (id: string) => {
    navigate(`/katas/${id}`)
  }

  const navigateToKataUpdate = (id: string) => {
    navigate(`/update/kata/${id}`)
  }

  useEffect(() => {
    getKatasByStars()
    getKatasByIntents()
}, [])

const ImageSrc = styled('span')({
  position: 'absolute',
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  backgroundSize: 'cover',
  backgroundPosition: 'center 40%',
});

  return (
    <ThemeProvider theme={myTheme}>
      <Box sx={{ display: 'flex' }}>
        <MenuDashboard />
        <Box component='main' sx={{ backgroundColor: (theme) => theme.palette.mode === 'light' ? theme.palette.grey[100] : theme.palette.grey[900], flexGrow: 1, height: '100vh', overflow: 'auto' }}>
          <Toolbar />
          <Container maxWidth='lg' sx={{ mt: 4, mg: 4 }}>
                        <Grid container spacing={2} maxWidth='lg'>
              <Grid item xs={12} md={8}  sm={12}>
                <Item sx={{minHeight: '602px'}}>
                <Typography variant='h4' color='' sx={{fontWeight: 900, textAlign: 'left', marginTop: 2, marginBottom: 2}}>Hey {userInfo.name}!<br/> it's great to see you!</Typography>
                     <Grid container spacing={2} sx={{ display: 'flex', flexDirection: 'column', marginBottom: 2}}>
                     <Grid item sx={{flexGrow: 1}}>
                     <Button variant="contained" color='success' sx={{width: '100%', height: 190, backgroundImage: `url(${dojo})`, backgroundSize: 'cover'}}>You made {userInfo.attempts} attempts</Button></Grid>
                    <Grid item sx={{flexGrow: 1}}>
                    <Button variant="contained" sx={{width: '100%', height: 190, backgroundImage: `url(${bambu})`, backgroundSize: 'cover'}}>You created {userInfo.katas} katas</Button></Grid>
                    </Grid>
                  <div style={{display: 'flex', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'center'}}><span style={{minWidth: 130}}><Typography variant='body1' sx={{fontWeight: 900, display: 'inline'}} onClick={() => navigate('/katas')}><IconButton color='primary'><SportsMartialArts /></IconButton>Try a Kata</Typography></span><span style={{minWidth: 130}}><Typography variant='body1' sx={{fontWeight: 900, display: 'inline'}} onClick={() => navigateToKataCreate()}><IconButton color='primary'><AddCircle /></IconButton>Create a new Kata</Typography></span>
                  <span style={{minWidth: 130}}><Typography variant='body1' sx={{fontWeight: 900, display: 'inline'}} onClick={() => navigateToUserUpdate(userInfo._id)}><IconButton color='primary'><Person /></IconButton>Update your info</Typography></span>{userInfo.role === 'Admin' ? <span style={{minWidth: 130}}><Typography variant='body1' sx={{fontWeight: 900, display: 'inline'}} onClick={() => navigate('/users')}><IconButton color='primary'><People /></IconButton>Users list</Typography></span> : null }</div>
                </Item>
              </Grid>
              <Grid item xs={12} md={4} sm={12}>
              <TableContainer component={Paper} sx={{ p: 2, display: 'flex', flexDirection: 'column', mb: 2 }}>
                <Typography variant='button'>Best rated katas</Typography>
                <Divider />
                <Table aria-label="simple table">
                  {katasByStars.length > 0 ? <TableBody>

                    {katasByStars.map((kata: Kata) => (
                      <TableRow key={kata._id} sx={{ '&:last-child td, &:last-child th': { border: 0 }, width: '100%' }} >
                      <TableCell sx={{width: '100%', display: 'flex', flexWrap: 'wrap'}}><div style={{display: 'inline-block'}}><Typography variant='body1' sx={{fontWeight: 900}} onClick={() => navigateToKataDetail(kata._id)}>{kata.name}</Typography><Typography variant='caption' >{kata.language.toUpperCase()} | {kata.level.toUpperCase()}</Typography></div>
                      <div style={{ flexGrow: 1, textAlign: 'right'}}><IconButton color='primary' ><Badge badgeContent={Math.round(kata.stars)} color='default'><Star /></Badge></IconButton><IconButton color='primary' ><Badge badgeContent={kata.intents} color='default'><SportsMartialArts /></Badge></IconButton></div></TableCell>
                      </TableRow>
                    ))}

                  </TableBody> : <div><h5>No katas found</h5></div>}
                </Table>
              </TableContainer>

              <TableContainer component={Paper} sx={{ p: 2, display: 'flex', flexDirection: 'column', mb: 2 }}>
              <Typography variant='button'>Most popular katas</Typography>
                <Divider />
                <Table aria-label="simple table">
                  {katasByIntents.length > 0 ? <TableBody>

                    {katasByIntents.map((kata: Kata) => (
                      <TableRow key={kata._id} sx={{ '&:last-child td, &:last-child th': { border: 0 }, width: '100%' }} >
                      <TableCell sx={{width: '100%', display: 'flex', flexWrap: 'wrap'}}><div style={{display: 'inline-block'}}><Typography variant='body1' sx={{fontWeight: 900}} onClick={() => navigateToKataDetail(kata._id)}>{kata.name}</Typography><Typography variant='caption' >{kata.language.toUpperCase()} | {kata.level.toUpperCase()}</Typography></div>
                      <div style={{ flexGrow: 1, textAlign: 'right'}}><IconButton color='primary' ><Badge badgeContent={Math.round(kata.stars)} color='default'><Star /></Badge></IconButton><IconButton color='primary' ><Badge badgeContent={kata.intents} color='default'><SportsMartialArts /></Badge></IconButton></div></TableCell>
                      </TableRow>
                    ))}

                  </TableBody> : <div><h5>No katas found</h5></div>}
                </Table>
              </TableContainer>
              </Grid>
                </Grid>
          </Container>
        </Box>
      </Box>
    </ThemeProvider >
  )
}