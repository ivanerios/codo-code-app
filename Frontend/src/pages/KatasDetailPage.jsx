import { AxiosResponse } from "axios";
import react, { useContext, useEffect, useState, Fragment } from "react";

import Editor from 'react-simple-code-editor';
import Highlight, { defaultProps, Language } from 'prism-react-renderer';
import theme from 'prism-react-renderer/themes/nightOwl';

import { useNavigate, useParams } from 'react-router-dom';
import MyContext from "../context/MyContext";

import { createTheme, ThemeProvider, Box, Toolbar, Container, Grid, Paper, Button, IconButton, Typography } from '@mui/material';
import { lightGreen } from '@mui/material/colors';
import { MenuDashboard } from '../components/dashboard/MenuDashboard';

import { useSessionStorage } from "../hooks/useSessionStorage";
import { getKataByID, updateKataByID } from "../services/katasService";
import { Kata } from "../utils/types/Kata.type";
import { Star } from "@mui/icons-material";

import { getUserByID, updateUserByID, updateUserWithoutPass } from "../services/usersService";

const styles = {
  root: {
    boxSizing: 'border-box',
    fontFamily: '"Dank Mono", "Fira Code", monospace',
    ...theme.plain
  }
}

const HighlightElement = (code, language) => (
  <Highlight {...defaultProps} theme={theme} code={code} language={language}>
    {({ className, style, tokens, getLineProps, getTokenProps }) => (
      <Fragment>
        {tokens.map((line, i) => (
          <div {...getLineProps({ line, key: i })}>
            {line.map((token, key) => <span {...getTokenProps({ token, key })} />)}
          </div>
        ))}
      </Fragment>
    )}
  </Highlight>
);

const myTheme = createTheme({
  palette: {
    primary: lightGreen,
    secondary: {
      main: '#FFFFFF',
    },
  },
});

export const KatasDetailPage = () => {

  const [code, setCode] = useState('');

    const styles = {
    root: {
      boxSizing: 'border-box',
      fontFamily: '"Dank Mono", "Fira Code", monospace',
      ...theme.plain
    }
  }

  const handleChange = (newCode) => {
    setCode(newCode);
  }

  const { userInfo, setUserInfo } = useContext(MyContext)

  let loggedIn = useSessionStorage('sessionJWTToken');
  let sessionIn = useSessionStorage('userId');
  let navigate = useNavigate();

  const [isAdmin, setIsAdmin] = useState(false)

  useEffect(() => {
    userInfo.role === 'Admin' ? setIsAdmin(true) : setIsAdmin(false)
  }, [isAdmin])

  let { id } = useParams();

  let kataInitialValues = {
    _id: '',
    name: '',
    description: '',
    stars: 1,
    level: '',
    intents: 0,
    creator: '',
    solution: '',
    participants: [''],
    language: ''
  }

  const [kata, setKata] = useState(kataInitialValues);

  const [showSolution, setShowSolution] = useState(false)

  useEffect(() => {
    if (!loggedIn) {
      return navigate('/login');
    } else {
      if (id) {
        getKataByID(loggedIn, id).then((response) => {
          if (response.status === 200 && response.data) {
            let kataData = {
              _id: response.data._id,
              name: response.data.name,
              description: response.data.description,
              stars: response.data.stars,
              level: response.data.level,
              intents: response.data.intents,
              creator: response.data.creator,
              solution: response.data.solution,
              participants: response.data.participants,
              language: response.data.language
            }

            setKata(kataData);
            setStarsInitial(response.data.stars);
            setCode(kataData.description);
          }

        }).catch((error) => console.error('Kata By ID error'))
      } else {
        return navigate('/katas');
      }

    }
  }, [loggedIn])

  const [stars, setStars] = useState(1)

  const [isRated, setIsRated] = useState(false)

  const starsIncrement = () => {
    if (stars < 5) {
      setStars(stars + 1)
    } else {
      setStars(1)
    }
  }

  const showTheSolution = () => {

    setShowSolution(true)

    setUser({
      ...user,
      attempts: user.attempts + 1
    })

    if (!Object.values(kata.participants).includes(userInfo._id)) {
      setKata({
        ...kata,
        participants: [...kata.participants, userInfo._id],
        intents: kata.intents + 1
      });
    } else {
      setKata({
        ...kata,
        intents: kata.intents + 1
      })
    }
  }

  const rateTheKata = () => {

      let intentsBase = kata.intents - 1;
      let multi = kata.stars * intentsBase;
      let sume = multi + stars;
      let result = sume / kata.intents;

      setKata({
        ...kata,
        stars: result
      });

    setIsRated(true)
  }

  const sendResults = () => {

    updateUserWithoutPass(loggedIn, user._id, user.name, user.email, user.role, user.age, user.katas, user.attempts)

    updateKataByID(loggedIn, kata._id, kata.name, kata.description, kata.level, kata.intents, kata.stars, kata.creator, kata.solution, kata.participants, kata.language).then(async (response) => {
      if (response.status == 200) {
        alert('Result sended correctly')
        navigate('/katas')
      } else {
        throw new Error('Error in sending')
      }
    }).catch((error) => console.error('Sending Error')
    )
  }

  let userInitialValues = 
    {_id: '',
      name: '',
      role: '',
      email: '',
      age: 18,
      katas: 0,
      attempts: 0,
    language: ''}

      const [user, setUser] = useState(userInitialValues)

  useEffect(() => {
    getUserByID(loggedIn, sessionIn).then(async (response) => {
      let userInform = {_id: response.data._id,
        name: response.data.name,
        role: response.data.role,
        email: response.data.email,
        age: response.data.age,
        katas: response.data.katas,
        attempts: response.data.attempts}

      await setUserInfo(userInform)
      await setUser(userInform)
    
    })
  }, [])

  const [starsInitial, setStarsInitial] = useState(1)

  return (
    <ThemeProvider theme={myTheme}>
      <Box sx={{ display: 'flex' }}>
        <MenuDashboard />
        <Box component='main' sx={{ backgroundColor: (theme) => theme.palette.mode === 'light' ? theme.palette.grey[100] : theme.palette.grey[900], flexGrow: 1, height: '100vh', overflow: 'auto' }}>
          <Toolbar />
          <Container maxWidth='lg' sx={{ mt: 4, mg: 4 }}>
            <Grid item xs={12} md={8} lg={9}>
              <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column', height: '85vh' }}>
                {kata ?
                  <div className="kata-data">
                    <Editor value={code} onValueChange={handleChange} highlight={HighlightElement} language={kata.language} padding={10} style={styles.root} />
                    <IconButton color='primary'><Star /></IconButton>
                    {starsInitial > 1.5 ? <IconButton color='primary'><Star /></IconButton> : <IconButton><Star color='disabled' /></IconButton>}
                    {starsInitial > 2.5 ? <IconButton color='primary'><Star /></IconButton> : <IconButton><Star color='disabled' /></IconButton>}
                    {starsInitial > 3.5 ? <IconButton color='primary'><Star /></IconButton> : <IconButton><Star color='disabled' /></IconButton>}
                    {starsInitial > 4.5 ? <IconButton color='primary'><Star /></IconButton> : <IconButton><Star color='disabled' /></IconButton>}
                    {!showSolution ? <Button sx={{
                      mt: 1,
                      mb: 1
                    }} fullWidth color="primary" variant="contained" onClick={() => showTheSolution()}>Check Solution</Button> : null}

                    {showSolution ?
                      <>
                      {code === kata.solution ? <Typography>You nailed it!</Typography> : <Typography>Seems that it's something wrong, check the solution, maybe it's our fault</Typography>}
                        <Editor value={kata.solution} highlight={HighlightElement} language={kata.language} padding={10} style={styles.root} />
                        <IconButton color='primary' onClick={starsIncrement}><Star /></IconButton>
                        {stars > 1.5 ? <IconButton color='primary' onClick={starsIncrement}><Star /></IconButton> : null}
                        {stars > 2.5 ? <IconButton color='primary' onClick={starsIncrement}><Star /></IconButton> : null}
                        {stars > 3.5 ? <IconButton color='primary' onClick={starsIncrement}><Star /></IconButton> : null}
                        {stars > 4.5 ? <IconButton color='primary' onClick={starsIncrement}><Star /></IconButton> : null}
                        {!isRated ? <Button sx={{
                          mt: 1,
                          mb: 1
                        }} fullWidth color="primary" variant="contained" onClick={rateTheKata}>Rate the Kata</Button> : <Button sx={{
                          mt: 1,
                          mb: 1
                        }} fullWidth color="primary" variant="contained" onClick={sendResults}>Send your result</Button>}
                      </>
                      : null}
                  </div> : <div>
                    <h2>Loading Data</h2></div>}
              </Paper>
            </Grid>
          </Container>
        </Box>
      </Box>
    </ThemeProvider>
  )
}