import { AxiosResponse } from "axios";
import react, { useContext, useEffect, useState } from "react";

import { useNavigate } from "react-router-dom";
import MyContext from "../context/MyContext";

import { createTheme, ThemeProvider, Box, Toolbar, Container, Grid, Paper, TableContainer, Table, TableBody, IconButton, TableRow, TableCell, Badge, InputLabel, Button, SelectChangeEvent, Select, MenuItem, Typography } from '@mui/material';
import { lightGreen } from '@mui/material/colors';
import { MenuDashboard } from '../components/dashboard/MenuDashboard';

import { useSessionStorage } from "../hooks/useSessionStorage";
import { deleteKataByID, getAllKatas, getAllKatasAttempted, getAllKatasByLanguage, getAllKatasSorted, getAllUserKatas } from "../services/katasService";
import { Kata } from "../utils/types/Kata.type";
import { AddCircle, ChevronLeft, ChevronRight, Delete, Edit, SportsKabaddi, SportsMartialArts, Star } from "@mui/icons-material";
import { getUserByID } from "../services/usersService";

const myTheme = createTheme({
  palette: {
    primary: lightGreen,
    secondary: {
      main: '#FFFFFF',
    },
  },
});

export const KatasPage = () => {

  const { userInfo, setUserInfo } = useContext(MyContext)

  let loggedIn = useSessionStorage('sessionJWTToken');
  let sessionIn = useSessionStorage('userId');
  let navigate = useNavigate();

  const [isAdmin, setIsAdmin] = useState<boolean>(false)

  useEffect(() => {
    userInfo.role === 'Admin' ? setIsAdmin(true) : setIsAdmin(false)
  }, [isAdmin])

  const [katas, setKatas] = useState([]);
  const [currentPageNumber, setCurrentPageNumber] = useState(1)
  const [totalPages, setTotalPages] = useState(1)

  const [language, setLanguage] = useState('')

  const previousPage = () => {
    setCurrentPageNumber(currentPageNumber - 1);
  }

  const nextPage = () => {
    setCurrentPageNumber(currentPageNumber + 1);
  }

  useEffect(() => {
    if (sort === 'all') {
      getKatasPaginated()
    } else if (sort === 'created') {
      getUserKatas()
    } else if (sort === 'attempted') {
      getKatasAttempted()
    } else if (sort === 'language') {
      getKatasByLanguage(language)
      }
  }, [currentPageNumber])

  const getKatasPaginated = () => {
    getAllKatas(loggedIn, 5, currentPageNumber).then((response: AxiosResponse) => {
      if (response.status === 200 && response.data.katas && response.data.currentPage && response.data.totalPages) {
        let { katas, totalPages } = response.data
        setKatas(katas);
        setTotalPages(totalPages);
      } else {
        throw new Error('Error obtaining Katas')
      }
    }).catch((error) => {console.error('Get All Katas Error')
    setKatas([]);
    setTotalPages(1);
  })
  }

  useEffect(() => {
      getKatasPaginated()
      }, [])

  const navigateToKataDetail = (id: string) => {
    navigate(`/katas/${id}`)
  }

  const navigateToKataUpdate = (id: string) => {
    navigate(`/update/kata/${id}`)
  }

  const deleteKata = (id: string) => {
    deleteKataByID(loggedIn, id).then(async (response: AxiosResponse) => {
      if (response.status === 200 && response.data) {
        navigate('/delete/kata')
      }
    }).catch((error) => console.error('Error deleting Kata By ID'))
    navigate('/delete/kata')
  }

  const navigateToKataCreate = () => {
    navigate('/create/kata')
  }

  const getUserKatas = () => {
    getAllUserKatas(loggedIn, 5, currentPageNumber, userInfo._id).then((response: AxiosResponse) => {
      if (response.status === 200 && response.data.katas && response.data.currentPage && response.data.totalPages) {
        let { katas, totalPages } = response.data
        setKatas(katas);
        setTotalPages(totalPages);
      } else {
        throw new Error('Error obtaining Katas')
      }
    }).catch((error) => {console.error('Get All Katas Error')
    setKatas([]);
    setTotalPages(1);
  })
  }

  const getKatasAttempted = () => {
    getAllKatasAttempted(loggedIn, 5, currentPageNumber, userInfo._id).then((response: AxiosResponse) => {
      if (response.status === 200 && response.data.katas && response.data.currentPage && response.data.totalPages) {
        let { katas, totalPages } = response.data
        setKatas(katas);
        setTotalPages(totalPages);
      } else {
        throw new Error('Error obtaining Katas')
      }
    }).catch((error) => {console.error('Get All Katas Error')
    setKatas([]);
    setTotalPages(1);
  })
  }

  const getKatasByLanguage = (language: string) => {
    getAllKatasByLanguage(loggedIn, 5, currentPageNumber, language).then((response: AxiosResponse) => {
      if (response.status === 200 && response.data.katas && response.data.currentPage && response.data.totalPages) {
        let { katas, totalPages } = response.data
        setKatas(katas);
        setTotalPages(totalPages);
      } else {
        throw new Error('Error obtaining Katas')
      }
    }).catch((error) => {console.error('Get All Katas Error')
    setKatas([]);
    setTotalPages(1);
  })
  }

  useEffect(() => {
    getUserByID(loggedIn, sessionIn).then(async (response: AxiosResponse) => {
      await setUserInfo({
        _id: response.data._id,
        name: response.data.name,
        role: response.data.role,
        email: response.data.email,
        age: response.data.age,
        katas: response.data.katas,
        attempts: response.data.attempts
      })
    })
  }, [])

  const [sort, setSort] = useState('all');

  const handleFormChange = (event: SelectChangeEvent) => {
    setSort(event.target.value);
  };

  useEffect(() => {
    if (sort === 'all') {
      getKatasPaginated()
    } else if (sort === 'created') {
      getUserKatas()
    } else if (sort === 'attempted') {
      getKatasAttempted()
    } else if (sort === 'language') {
      setLanguage('javascript')
      getKatasByLanguage(language)
      }
    }, [sort])
  
  const handleLanguageChange = (event: SelectChangeEvent) => {
    setLanguage(event.target.value);
      };

  useEffect(() => {
    getKatasByLanguage(language);
  }, [language])
  
  return (
    <ThemeProvider theme={myTheme}>
      <Box sx={{ display: 'flex' }}>
        <MenuDashboard />
        <Box component='main' sx={{ backgroundColor: (theme) => theme.palette.mode === 'light' ? theme.palette.grey[100] : theme.palette.grey[900], flexGrow: 1, height: '100vh', overflow: 'auto' }}>
          <Toolbar />
          <Container maxWidth='lg' sx={{ mt: 4, mg: 4 }}>
            <Grid item xs={12} md={8} lg={9}>

              <Select sx={{
                mt: 1,
                mb: 1
              }}
                fullWidth
                id='sort'
                value={sort}
                onChange={handleFormChange}>
                <MenuItem value={'all'}>All</MenuItem>
                <MenuItem value={'created'}>Created by User</MenuItem>
                <MenuItem value={'attempted'}>Attempted by User</MenuItem>
                <MenuItem value={'language'}>By Language</MenuItem>
              </Select>

              {sort === 'language' ? <Select sx={{
                mt: 1,
                mb: 1
              }}
                fullWidth
                id='language'
                value={language}
                onChange={handleLanguageChange}>
                <MenuItem value={'javascript'}>Javascript</MenuItem>
                <MenuItem value={'typescript'}>Typescript</MenuItem>
                <MenuItem value={'csharp'}>C#</MenuItem>
                <MenuItem value={'go'}>Go</MenuItem>
                <MenuItem value={'java'}>Java</MenuItem>
                <MenuItem value={'kotlin'}>Kotlin</MenuItem>
                <MenuItem value={'php'}>PHP</MenuItem>
                <MenuItem value={'ruby'}>Ruby</MenuItem>
                <MenuItem value={'jsx'}>JSX</MenuItem>
                <MenuItem value={'tsx'}>TSX</MenuItem>
              </Select> : null}

              <TableContainer component={Paper} sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
                <Table aria-label="simple table">
                  {katas.length > 0 ? <TableBody>

                    {katas.map((kata: Kata) => (
                      <TableRow key={kata._id} sx={{ '&:last-child td, &:last-child th': { border: 0 }, width: '100%' }} >
                        <TableCell sx={{width: '100%', display: 'flex', flexWrap: 'wrap'}}><div style={{display: 'inline-block'}}><Typography variant='body1' sx={{fontWeight: 900}} onClick={() => navigateToKataDetail(kata._id)}>{kata.name}</Typography><Typography variant='caption' >{kata.language.toUpperCase()} | {kata.level.toUpperCase()}</Typography></div>
                        <div style={{ flexGrow: 1, textAlign: 'right'}}>{kata.participants.includes(userInfo._id) ? <IconButton color='primary'><SportsKabaddi /></IconButton> : <IconButton><SportsKabaddi color='disabled' /></IconButton>}<IconButton color='primary' ><Badge badgeContent={Math.round(kata.stars)} color='default'><Star /></Badge></IconButton><IconButton color='primary' ><Badge badgeContent={kata.intents} color='default'><SportsMartialArts /></Badge></IconButton>
                          {userInfo._id === kata.creator || userInfo.role === 'Admin' ? <span><IconButton color='primary' onClick={() => navigateToKataUpdate(kata._id)}><Edit /></IconButton><IconButton color='primary' onClick={() => deleteKata(kata._id)}><Delete /></IconButton></span> : null}</div></TableCell>
                      </TableRow>
                    ))}

                  </TableBody> : <div><h5>No katas found</h5></div>}
                </Table>
              </TableContainer>
              <span>Page {currentPageNumber}</span>
              {currentPageNumber > 1 ? <IconButton color='inherit' onClick={previousPage}><ChevronLeft /></IconButton> : <IconButton color='inherit'><ChevronLeft /></IconButton>}
              {currentPageNumber < totalPages ? <IconButton color='inherit' onClick={nextPage}><ChevronRight /></IconButton> : <IconButton color='inherit'><ChevronRight /></IconButton>}
              <IconButton onClick={() => navigateToKataCreate()} color='primary'><AddCircle /></IconButton>
            </Grid>
          </Container>
        </Box>
      </Box>
    </ThemeProvider>
  )
}