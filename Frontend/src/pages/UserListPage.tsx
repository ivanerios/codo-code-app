import { AxiosResponse } from "axios";
import react, { useContext, useEffect, useState } from "react";

import { useNavigate } from "react-router-dom";
import MyContext from "../context/MyContext";

import { createTheme, ThemeProvider, Box, Toolbar, Container, Grid, Paper, IconButton, TableContainer, Table, TableBody, TableRow, TableCell, Typography } from '@mui/material';
import { lightGreen } from '@mui/material/colors';
import { MenuDashboard } from '../components/dashboard/MenuDashboard';

import { useSessionStorage } from "../hooks/useSessionStorage";
import { deleteUserByID, getAllUsers, getUserByID } from "../services/usersService";
import { User } from "../utils/types/User.type";
import { AddCircle, ChevronLeft, ChevronRight, Delete, Edit, Person } from "@mui/icons-material";

const myTheme = createTheme({
  palette: {
    primary: lightGreen,
    secondary: {
      main: '#FFFFFF',
    },
  },
});

export const UserListPage = () => {

  const { userInfo, setUserInfo } = useContext(MyContext)

  let loggedIn = useSessionStorage('sessionJWTToken');
  let sessionIn = useSessionStorage('userId');
  let navigate = useNavigate();

  const [isAdmin, setIsAdmin] = useState<boolean>(false)

  useEffect(() => {
    userInfo.role === 'Admin' ? setIsAdmin(true) : setIsAdmin(false)
  }, [isAdmin])

  const [users, setUsers] = useState([]);
  const [currentPageNumber, setCurrentPageNumber] = useState(1)
  const [totalPages, setTotalPages] = useState(1)

const previousPage = () => {
    setCurrentPageNumber(currentPageNumber - 1);
  }

  const nextPage = () => {
      setCurrentPageNumber(currentPageNumber + 1);
  }

  useEffect(() => {
    getUserPaginated();
  }, [currentPageNumber])
  

  const getUserPaginated = () => {
    getAllUsers(loggedIn, 5, currentPageNumber).then((response: AxiosResponse) => {
      if (response.status === 200 && response.data.users && response.data.currentPage && response.data.totalPages) {
        let { users, totalPages } = response.data
        setUsers(users);
        setTotalPages(totalPages);
      } else {
        throw new Error('Error obtaining Users')
      }
    }).catch((error) => console.error('Get All Users Error'))
  }

  useEffect(() => {
    if (!loggedIn) {
      return navigate('/login');
    } else {
      getUserPaginated();
    }
  }, [loggedIn])

  const navigateToUserUpdate = (id: string) => {
    navigate(`/update/user/${id}`)
  }

  const deleteUser = (id: string) => {
    deleteUserByID(loggedIn, id).then(async (response: AxiosResponse) => {
      if (response.status === 200 && response.data) {
        navigate('/delete/user')
      }
    }).catch((error) => console.error('Error deleting Kata By ID'))
    navigate('/delete/user')
  }

  const navigateToUserCreate = () => {
    navigate('/create/user')
  }

  useEffect(() => {
    getUserByID(loggedIn, sessionIn).then(async (response: AxiosResponse) => {
      await setUserInfo({_id: response.data._id,
      name: response.data.name,
      role: response.data.role,
      email: response.data.email,
      age: response.data.age,
      katas: response.data.katas,
      attempts: response.data.attempts})})
  }, [])

  return (
    <ThemeProvider theme={myTheme}>
      <Box sx={{ display: 'flex' }}>
        <MenuDashboard />
        <Box component='main' sx={{ backgroundColor: (theme) => theme.palette.mode === 'light' ? theme.palette.grey[100] : theme.palette.grey[900], flexGrow: 1, height: '100vh', overflow: 'auto' }}>
          <Toolbar />
          <Container maxWidth='lg' sx={{ mt: 4, mg: 4 }}>
            <Grid item xs={12} md={8} lg={9}>
              <TableContainer component={Paper} sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
                <Table aria-label="simple table">

                  {users.length > 0 ? <TableBody>

                    {users.map((user: User) => (
                      <TableRow key={user._id} sx={{ '&:last-child td, &:last-child th': { border: 0 } }} >
                          <TableCell sx={{width: '100%', display: 'flex', flexWrap: 'wrap'}}><div style={{display: 'inline-block'}}><Typography variant='body1' sx={{fontWeight: 900}}>{user.name}</Typography><Typography variant='caption' >{user.role.toUpperCase()} | {user.email.toUpperCase()}</Typography></div>
                        {user.role !== 'Admin' ? <div style={{ flexGrow: 1, textAlign: 'right'}}><IconButton onClick={() => navigateToUserUpdate(user._id)}><Edit color='primary' /></IconButton><IconButton color='primary'  onClick={() => deleteUser(user._id)} ><Delete /></IconButton></div> : <div style={{ flexGrow: 1, textAlign: 'right'}}><IconButton color='primary'  onClick={() => navigateToUserUpdate(user._id)}><Edit /></IconButton><IconButton><Delete color='disabled' /></IconButton></div>}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                    : <div><h5>No users found</h5></div>}
                </Table>
              </TableContainer>
              <span>Page {currentPageNumber}</span>
              {currentPageNumber > 1 ? <IconButton color='inherit' onClick={previousPage}><ChevronLeft /></IconButton> : <IconButton color='inherit'><ChevronLeft /></IconButton>}
              {currentPageNumber < totalPages ? <IconButton color='inherit' onClick={nextPage}><ChevronRight /></IconButton> : <IconButton color='inherit'><ChevronRight /></IconButton>}
              <IconButton onClick={() => navigateToUserCreate()} color='primary'><AddCircle /></IconButton>
            </Grid>
          </Container>
        </Box>
      </Box>
    </ThemeProvider>
  )
}