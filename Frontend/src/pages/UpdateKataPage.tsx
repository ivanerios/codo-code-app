import { AxiosResponse } from "axios";
import react, { useContext, useEffect, useState } from "react";

import { useNavigate, useParams } from "react-router-dom";
import MyContext from "../context/MyContext";

import { createTheme, ThemeProvider, Box, Toolbar, Container, Grid, Paper } from '@mui/material';
import { lightGreen } from '@mui/material/colors';
import { MenuDashboard } from '../components/dashboard/MenuDashboard';

import { useSessionStorage } from "../hooks/useSessionStorage";
import { getKataByID } from "../services/katasService";
import { Kata } from "../utils/types/Kata.type";
import UpdateKataForm from "../components/forms/UpdateKataForm";
import { getUserByID } from "../services/usersService";

const myTheme = createTheme({
  palette: {
    primary: lightGreen,
    secondary: {
      main: '#FFFFFF',
    },
  },
});

export const UpdateKataPage = () => {

  const { userInfo, setUserInfo } = useContext(MyContext)

  let loggedIn = useSessionStorage('sessionJWTToken');
  let sessionIn = useSessionStorage('userId');
  let navigate = useNavigate();

  const [isAdmin, setIsAdmin] = useState<boolean>(false)

  useEffect(() => {
    userInfo.role === 'Admin' ? setIsAdmin(true) : setIsAdmin(false)
  }, [isAdmin])

  let { id } = useParams();

  const [kata, setKata] = useState<Kata | undefined>(undefined);

  useEffect(() => {
    if (!loggedIn) {
      return navigate('/login');
    } else {
      if (id) {
        getKataByID(loggedIn, id).then((response: AxiosResponse) => {
          if (response.status === 200 && response.data) {
            let kataData: Kata = {
              _id: response.data._id,
              name: response.data.name,
              description: response.data.description,
              stars: response.data.stars,
              level: response.data.level,
              intents: response.data.intents,
              creator: response.data.creator,
              solution: response.data.solution,
              participants: response.data.participants,
              language: response.data.language
            }

            setKata(kataData);

            console.table(kata)
          }

        }).catch((error) => console.error('Kata By ID error'))
      } else {
        return navigate('/katas');
      }

    }
  }, [loggedIn])

  useEffect(() => {
    getUserByID(loggedIn, sessionIn).then(async (response: AxiosResponse) => {
      await setUserInfo({_id: response.data._id,
      name: response.data.name,
      role: response.data.role,
      email: response.data.email,
      age: response.data.age,
      katas: response.data.katas,
      attempts: response.data.attempts})})
  }, [])

  return (
    <ThemeProvider theme={myTheme}>
      <Box sx={{ display: 'flex' }}>
        <MenuDashboard />
        <Box component='main' sx={{ backgroundColor: (theme) => theme.palette.mode === 'light' ? theme.palette.grey[100] : theme.palette.grey[900], flexGrow: 1, height: '100vh', overflow: 'auto' }}>
          <Toolbar />
          <Container maxWidth='lg' sx={{ mt: 4, mg: 4 }}>
            <Grid item xs={12} md={8} lg={9}>
              <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column'}}>
                <UpdateKataForm />



              </Paper>
            </Grid>
          </Container>
        </Box>
      </Box>
    </ThemeProvider>
  )
}