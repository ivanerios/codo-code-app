import { AxiosResponse } from "axios";
import react, { useContext, useEffect, useState } from "react";

import { useNavigate } from "react-router-dom";
import MyContext from "../context/MyContext";

import { createTheme, ThemeProvider, Box, Toolbar, Container, Grid, Paper, TableContainer, Table, TableBody, IconButton, TableRow, TableCell, Badge, Button, MenuItem, Select, SelectChangeEvent, Typography } from '@mui/material';
import { lightGreen } from '@mui/material/colors';
import { MenuDashboard } from '../components/dashboard/MenuDashboard';

import { useSessionStorage } from "../hooks/useSessionStorage";
import { getAllKatas, getAllKatasSorted } from "../services/katasService";
import { Kata } from "../utils/types/Kata.type";
import { AddCircle, ChevronLeft, ChevronRight, Delete, Edit, Person, SportsMartialArts, Star } from "@mui/icons-material";
import { getUserByID } from "../services/usersService";

const myTheme = createTheme({
  palette: {
    primary: lightGreen,
    secondary: {
      main: '#FFFFFF',
    },
  },
});

export const RankingPage = () => {

  const { userInfo, setUserInfo } = useContext(MyContext)

  let loggedIn = useSessionStorage('sessionJWTToken');
  let sessionIn = useSessionStorage('userId');
  let navigate = useNavigate();

  const [isAdmin, setIsAdmin] = useState<boolean>(false)

  useEffect(() => {
    userInfo.role === 'Admin' ? setIsAdmin(true) : setIsAdmin(false)
  }, [isAdmin])

  const [katas, setKatas] = useState([]);
  const [currentPageNumber, setCurrentPageNumber] = useState(1)
  const [totalPages, setTotalPages] = useState(1)

  const previousPage = () => {
    setCurrentPageNumber(currentPageNumber - 1);
  }

  const nextPage = () => {
    setCurrentPageNumber(currentPageNumber + 1)
  }

  useEffect(() => {
    if (sort === 'rated') {
    getKatasByStars();
    } else {
      getKatasByIntents();
    }
  }, [currentPageNumber])

  useEffect(() => {
      getKatasByStars()
  }, [])

  const navigateToKataDetail = (id: string) => {
    navigate(`/katas/${id}`)
  }

  const navigateToKataUpdate = (id: string) => {
    navigate(`/update/kata/${id}`)
  }

  const navigateToKataCreate = () => {
    navigate('/create/kata')
  }

  const getKatasByIntents = () => {
    getAllKatasSorted(loggedIn, 5, currentPageNumber, 'intents').then((response: AxiosResponse) => {
      if (response.status === 200 && response.data.katas && response.data.currentPage && response.data.totalPages) {
        let { katas, totalPages } = response.data
        setKatas(katas);
        setTotalPages(totalPages);
      } else {
        throw new Error('Error obtaining Katas')
      }
    }).catch((error) => console.error('Get All Users Error'))
  }

  const getKatasByStars = () => {
    getAllKatasSorted(loggedIn, 5, currentPageNumber, 'stars').then((response: AxiosResponse) => {
      if (response.status === 200 && response.data.katas && response.data.currentPage && response.data.totalPages) {
        let { katas, totalPages } = response.data
        setKatas(katas);
        setTotalPages(totalPages);
      } else {
        throw new Error('Error obtaining Katas')
      }
    }).catch((error) => console.error('Get All Users Error'))
  }

  useEffect(() => {
    getUserByID(loggedIn, sessionIn).then(async (response: AxiosResponse) => {
      await setUserInfo({_id: response.data._id,
      name: response.data.name,
      role: response.data.role,
      email: response.data.email,
      age: response.data.age,
      katas: response.data.katas,
      attempts: response.data.attempts})})
  }, [])

  const [sort, setSort] = useState('rated');

  const handleFormChange = (event: SelectChangeEvent) => {
    setSort(event.target.value);
  };

  useEffect(() => {
    if (sort === 'rated') {
      getKatasByStars();
    } else {
      getKatasByIntents();
    }
    }, [sort])

  return (
    <ThemeProvider theme={myTheme}>
      <Box sx={{ display: 'flex' }}>
        <MenuDashboard />
        <Box component='main' sx={{ backgroundColor: (theme) => theme.palette.mode === 'light' ? theme.palette.grey[100] : theme.palette.grey[900], flexGrow: 1, height: '100vh', overflow: 'auto' }}>
          <Toolbar />
          <Container maxWidth='lg' sx={{ mt: 4, mg: 4 }}>
            <Grid item xs={12} md={8} lg={9}>
            <Select sx={{
                mt: 1,
                mb: 1
              }}
                fullWidth
                id='sort'
                value={sort}
                onChange={handleFormChange}>
                <MenuItem value={'rated'}>Best Rated</MenuItem>
                <MenuItem value={'attempted'}>Most Popular</MenuItem>
               </Select>
              <TableContainer component={Paper} sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
                <Table aria-label="simple table">
                  {katas.length > 0 ? <TableBody>

                    {katas.map((kata: Kata) => (
                      <TableRow key={kata._id} sx={{ '&:last-child td, &:last-child th': { border: 0 }, width: '100%' }} >
                      <TableCell sx={{width: '100%', display: 'flex', flexWrap: 'wrap'}}><div style={{display: 'inline-block'}}><Typography variant='body1' sx={{fontWeight: 900}} onClick={() => navigateToKataDetail(kata._id)}>{kata.name}</Typography><Typography variant='caption' >{kata.language.toUpperCase()} | {kata.level.toUpperCase()}</Typography></div>
                      <div style={{ flexGrow: 1, textAlign: 'right'}}><IconButton color='primary' ><Badge badgeContent={Math.round(kata.stars)} color='default'><Star /></Badge></IconButton><IconButton color='primary' ><Badge badgeContent={kata.intents} color='default'><SportsMartialArts /></Badge></IconButton></div></TableCell>
                      </TableRow>
                    ))}

                  </TableBody> : <div><h5>No katas found</h5></div>}
                </Table>
              </TableContainer>
              <span>Page {currentPageNumber}</span>
              {currentPageNumber > 1 ? <IconButton color='inherit' onClick={previousPage}><ChevronLeft /></IconButton> : <IconButton color='inherit'><ChevronLeft /></IconButton>}
              {currentPageNumber < totalPages ? <IconButton color='inherit' onClick={nextPage}><ChevronRight /></IconButton> : <IconButton color='inherit'><ChevronRight /></IconButton>}
              <IconButton onClick={() => navigateToKataCreate()} color='primary'><AddCircle /></IconButton>
            </Grid>
          </Container>
        </Box>
      </Box>
    </ThemeProvider>
  )
}