export type User = {
  _id: string,
  name: string,
  email: string,
  role: string,
  age: number,
  katas: number,
  attempts: number
}

export type UserValues = {
  name: string | undefined,
  email: string | undefined,
  age: number | undefined
}