import { AxiosRequestConfig } from 'axios';
import axios from '../utils/config/axios.config';

export const getAllUsers = (token: string, limit?: number, page?: number) => {
  const options: AxiosRequestConfig = { headers: { 'x-access-token': token }, params: { limit, page } };
  return axios.get('/users', options)
}

export const getUserByID = (token: string, id: string) => {
  const options: AxiosRequestConfig = { headers: { 'x-access-token': token }, params: { id } };
  return axios.get('/users', options)
}

export const updateUserByID = (token: string, id: string | undefined, name?: string | undefined, email?: string | undefined, role?: string | undefined, age?: number | undefined, password?: string | undefined, katas?: number | undefined, attempts?: number | undefined) => {

  let body = {
    name: name,
    email: email,
    role: role,
    age: age,
    password: password,
    katas: katas,
    attempts: attempts
  }

  const options: AxiosRequestConfig = { headers: { 'x-access-token': token }, params: { id } };

  console.log(body, options)

  return axios.put('/users', body, options)
}

export const updateUserWithoutPass = (token: string, id: string | undefined, name?: string | undefined, email?: string | undefined, role?: string | undefined, age?: number | undefined, katas?: number | undefined, attempts?: number | undefined) => {

  let body = {
    name: name,
    email: email,
    role: role,
    age: age,
    katas: katas,
    attempts: attempts
  }

  const options: AxiosRequestConfig = { headers: { 'x-access-token': token }, params: { id } };

  console.log(body, options)

  return axios.put('/users', body, options)
}

export const deleteUserByID = (token: string, id: string) => {
  const options: AxiosRequestConfig = { headers: { 'x-access-token': token }, params: { id } };
  return axios.delete('/users', options)
}