import { AxiosRequestConfig } from 'axios';
import axios from '../utils/config/axios.config';

export const getAllKatas = (token: string, limit?: number, page?: number) =>{
    const options: AxiosRequestConfig = { headers: {'x-access-token': token}, params: {limit, page}};
return axios.get('/katas', options)
}

export const getAllUserKatas = (token: string, limit?: number, page?: number, creator?: string) =>{
  const options: AxiosRequestConfig = { headers: {'x-access-token': token}, params: {limit, page, creator}};
return axios.get('/katas', options)
}

export const getAllKatasAttempted = (token: string, limit?: number, page?: number, participant?: string) =>{
  const options: AxiosRequestConfig = { headers: {'x-access-token': token}, params: { limit, page, participant}};
return axios.get('/katas', options)
}

export const getAllKatasByLanguage = (token: string, limit?: number, page?: number, language?: string) =>{
  const options: AxiosRequestConfig = { headers: {'x-access-token': token}, params: { limit, page, language}};
return axios.get('/katas', options)
}

export const getAllKatasSorted = (token: string, limit?: number, page?: number, sort?: string) =>{
  const options: AxiosRequestConfig = { headers: {'x-access-token': token}, params: {limit, page, sort}};
return axios.get('/katas', options)
}

export const getKataByID = (token: string, id: string) =>{
    const options: AxiosRequestConfig = { headers: {'x-access-token': token}, params: {id}};
return axios.get('/katas', options)
}

export const createKata = (token: string, name?: string | undefined, description?: string | undefined, level?: string | undefined, intents?: number | undefined, stars?: number | undefined, creator?: string | undefined,  solution?: string | undefined, participants?: string[] | undefined, language?: string | undefined) => {

  let body = {
    name: name,
    description: description,
    level: level,
    intents: intents,
    stars: stars,
    creator: creator,
    solution: solution,
    participants: participants,
    language: language
  }

  const options: AxiosRequestConfig = { headers: { 'x-access-token': token }};

  return axios.post('/katas', body, options)
}

export const updateKataByID = (token: string, id: string | undefined, name?: string | undefined, description?: string | undefined, level?: string | undefined, intents?: number | undefined, stars?: number | undefined, creator?: string | undefined,  solution?: string | undefined, participants?: string[] | undefined, language?: string | undefined) => {

  let body = {
    name: name,
    description: description,
    level: level,
    intents: intents,
    stars: stars,
    creator: creator,
    solution: solution,
    participants: participants,
    language: language
  }

  const options: AxiosRequestConfig = { headers: { 'x-access-token': token }, params: { id } };

  return axios.put('/katas', body, options)
}

export const deleteKataByID = (token: string, id: string) =>{
  const options: AxiosRequestConfig = { headers: {'x-access-token': token}, params: {id}};
return axios.delete('/katas', options)
}