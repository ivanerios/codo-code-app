import { AxiosResponse } from 'axios';
import { useNavigate } from 'react-router-dom';
import { string } from 'yup';
import axios from '../utils/config/axios.config';




/**
 * Login method
 * @param {string} email Email to login a user
 * @param {string} password Password to login a user
 * @returns 
 */
export const login = (email: string, password: string) => {

  // Declare Body to POST

  let body = {
    email: email,
    password: password
  }

  // Send POST request to login

  return axios.post('/auth/login', body)

}

/**
 * Register method
 * @param {string} name Name to register a user
 * @param {string} role Role to register a user, always 'User' by default
 * @param {string} email Email to register a user
 * @param {string} password Password to register a user
 * @param {number} age Age to register a user
 * @returns 
 */

export const register = (name: string, role: string, email: string, password: string, age: number, katas: number, attempts: number) => {
  
  let body = {
    name: name,
    role: role,
    email: email,
    password: password,
    age: age,
    katas: katas,
    attempts: attempts
  }
  return axios.post('/auth/register', body)
}